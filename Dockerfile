FROM node
# Recuperation de l'image officielle node comme base

# Nous créons un répertoire pour contenir le code de l'application dans l'image. Ce sera le r
WORKDIR /usr/src/app

# On copie les fichiers dont on a besoin à l'interieur de notre image
COPY package*.json ./

# On installe toutes les dépendances
RUN npm install

# On récupère notre fichier index.js
COPY . ./

# On indique au daemon que l'on veut lancer notre docker sur le port 80
EXPOSE 8080

# On lance la commande npm start pour démarrer notre application node
CMD [ "npm", "test" ]
